# -*- coding: utf-8 -*-
"""
Created on Fri Jul  8 14:00:34 2022

@author: User
"""

import os
import matplotlib.pyplot as plt

def create_path(fold,arb):
    cur_dir = os.getcwd()
    if os.path.exists(fold):
        os.chdir(fold)
        cur_now = fold
        for k in arb:
            cur_new = cur_now+'/' + k
            if not os.path.exists(cur_new):
                os.makedirs(cur_new)
            os.chdir(fold)
            cur_now = cur_new
    
    else:
        print('%s does not exist, please check and create' %(fold))
    os.chdir(cur_dir)       
    return cur_new



def plot_scatter(x,y,s,xlab,ylab):
    plt.scatter(x, y,s, alpha=0.5)
    
    plt.xlabel(xlab)
    plt.ylabel(ylab)
    plt.legend(loc='upper left')
    plt.show()
            