# -*- coding: utf-8 -*-
"""
Created on Tue Jul 19 15:19:40 2022

@author: User
"""

import pandas as pd
import datetime as dt
from ib_tools import read_positions
from bbg import bbg_hist, get_volatility
from local_paths import get_icloud_path

IB_BBG_ref = get_icloud_path()+'/params/IB_BBG.xls'


def IB_BBG_exchange(data_in,fld_exc = 'Exchange', ib_to_bbg = True):
    lut = pd.read_excel(IB_BBG_ref, sheet_name='exchanges', keep_default_na=False)
    if ib_to_bbg:
        lut = lut.rename(columns={'IB' : fld_exc, 'BBG':'BBG_exch'})
        fld_out = 'BBG_exch'
    else:
        lut = lut.rename(columns={'BBG' : fld_exc, 'IB':'IB_exch'})
        fld_out = 'IB_exch'
    data_in = data_in.merge(lut, on=fld_exc, how='left')    
    if any(data_in[fld_out].isna()):
        print('No venues found for the following')
        print(data_in[data_in[fld_out].isna()])
        print('please, add in ', IB_BBG_ref)
    return data_in

def IB_BBG_ticker(data_in, fld_symbol = 'Symbol', fld_exc = 'Exchange', ib_to_bbg = True):
    data_in = IB_BBG_exchange(data_in, fld_exc = fld_exc, ib_to_bbg = ib_to_bbg)
    if ib_to_bbg:
        data_in[fld_symbol+'_bbg'] = [x.replace('.','') for x in data_in[fld_symbol]]
        data_in['Ticker'] = data_in[fld_symbol+'_bbg'] + ' ' + data_in['BBG_exch']
        
    return data_in

def get_positions_bbg():
    all_pos = read_positions()
    all_pos = IB_BBG_ticker(all_pos, fld_symbol = 'Symbol', fld_exc = 'Exchange', ib_to_bbg = True)
    
    
    px = bbg_hist(all_pos['Ticker'],'PX_LAST',[dt.date.today()], FX='')
    
    all_pos = all_pos.merge(px, on='Ticker')
    all_pos.loc[all_pos['Currency'] =='GBP','PX_LAST'] = all_pos[all_pos['Currency'] =='GBP']['PX_LAST']/100
    
    all_pos['px_perf'] = all_pos['PX_LAST'] / all_pos['Average Cost'] -1
    all_pos['pnl_local'] = all_pos['Quantity'] * (all_pos['PX_LAST'] - all_pos['Average Cost'])
    
    all_pos['TickCurrency'] = [x +'USD' for x in all_pos['Currency']]
    fx = all_pos['TickCurrency'].drop_duplicates().tolist()
    fx.remove('USDUSD')
    fx_last = bbg_hist(fx,'PX_LAST',[dt.date.today()], FX='',type_in='fx').drop(columns=['date']).\
        rename(columns={'Ticker':'TickCurrency' , 'PX_LAST':'FX'})
    fx_last = pd.concat([fx_last, pd.DataFrame({'TickCurrency':['USDUSD'], 'FX':[1]})])
    
    
    
    all_pos = all_pos.merge(fx_last, on ='TickCurrency').drop(columns=['TickCurrency'])
    all_pos['Size'] = all_pos['Quantity'] * all_pos['PX_LAST'] * all_pos['FX']
    all_pos['pnl_usd'] = all_pos['pnl_local'] * all_pos['FX']
    
    volat = get_volatility(all_pos['Ticker'], dt.date.today(),FX='', nb_ref = 7*5)
    all_pos = all_pos.merge(volat, on = 'Ticker')
    all_pos['Risk_Arb'] = all_pos['ann_vol'] <0.1
    
    long_size = all_pos[all_pos['Size']>0]['Size'].sum()
    short_size = all_pos[all_pos['Size']<0]['Size'].sum()
    net_size = all_pos['Size'].sum()
    risk_arb_size = all_pos[all_pos['Risk_Arb']]['Size'].sum()
    pnl = all_pos['pnl_usd'].sum()
    risk_arb_pnl = all_pos[all_pos['Risk_Arb']]['pnl_usd'].sum()
    
    print('Long: %d / Short: %d, / Net: %d' %(int(long_size), int(short_size), int(net_size)))
    print('Risk arb: %d / Net without risk arb: %d'%(int(risk_arb_size), int(net_size-risk_arb_size)))
    print('pnl: %d'%(int(pnl) ))
    print('Risk arb pnl: %d' % int(risk_arb_pnl))
    
    return all_pos