# -*- coding: utf-8 -*-
"""
Created on Wed Jul  6 22:13:51 2022

@author: User
"""
import os
import pandas as pd
import pdblp
import datetime as dt
from tools import create_path, plot_scatter
from pandas.tseries.offsets import  BDay
from local_paths import get_docs_path
from dates_calendar import get_business_day

IDX_path = get_docs_path() + 'BBG/idx'
Universe_path = get_docs_path() + 'BBG/universe'

min_flow_extra = 5


def bbg_prepare_ticker(ticker, type_in='ticker', wayin=True):
    if type_in.lower() == 'ticker':
        res_in = [x + ' Equity' for x in ticker]
        res_out = [x.replace(' Equity','') for x in ticker]
    
    if type_in.lower() == 'fx':
        res_in = [x + ' Curncy' for x in ticker]
        res_out = [x.replace(' Curncy','') for x in ticker]
        
    if wayin == True:
        return res_in
    else:
        return res_out

def bbg_hist(ticker,fld,dates, FX='', type_in = 'ticker'):
    con = pdblp.BCon(debug=False, port=8194, timeout=5000)
    con.start()
    
    tick_req = bbg_prepare_ticker(ticker,  type_in=type_in, wayin=True)
    
    if isinstance(dates, dt.date):
        dates = [dates]
    
    if FX in ['',None]:
        elems = None

    else:
        elems = [('currency',FX)]

    # import pdb;pdb.set_trace()
    res = con.bdh(tick_req, fld,
            min(dates).strftime('%Y%m%d'), max(dates).strftime('%Y%m%d'), elms = elems)
    con.stop()
    
    res = res.transpose().stack().reset_index().pivot(index=['ticker', 'date'], columns='field', values = 0)
    
    res = res.reset_index().rename(columns={'ticker':'Ticker'})
   
    res['Ticker'] = bbg_prepare_ticker(res['Ticker'],  type_in=type_in, wayin=False)
    res['date'] = [x.date() for x in res['date']]
    return res

def bbg_ref(ticker,fld):
    con = pdblp.BCon(debug=False, port=8194, timeout=5000)
    con.start()
    
    res = con.ref(ticker, fld)
    con.stop()
    
    return res

def bbg_idx(ticker,dates):
    con = pdblp.BCon(debug=False, port=8194, timeout=8500)
    
    con.start()
    if isinstance(ticker, str):
        ticker = [ticker+' Index']
    else:
        ticker = [x + ' Index' for x in ticker]
        
    if not isinstance(dates,list):
        dates = [dates]
    res = pd.DataFrame()
    for k in dates:
        rescur = con.bulkref(ticker, 'INDX_MWEIGHT_HIST' ,[("END_DATE_OVERRIDE",k.strftime('%Y%m%d')),]             )
        rescur['date'] = k
        if res.shape[0] == 0:
            res = rescur
        else:
            res = pd.concat([res,rescur])
    con.stop()
    
    # import pdb;pdb.set_trace()
    res = res.drop(columns=['field'])
    res2 = res.pivot(values = 'value', index =['ticker','date', 'position'], columns ='name').reset_index().drop(columns=['position'])
    res2 = res2.rename(columns={'ticker':'Index', 'Index Member':'Ticker', 'Percent Weight':'Wgt'})
    res2['Index'] = [x.replace(' Index','') for x in res2['Index']]
    return res2


def bbg_idx_delta(ticker,dates, volumes=False, FX='USD', country='UK'):
    res = pd.DataFrame()
    
    if isinstance(dates,dt.date):
        dates = [dates, get_business_day(dates+BDay(1), next_date=1, country=country)]
    
    dates.sort()
    all_idx = bbg_idx(ticker,dates)
    changes = pd.DataFrame()    
    
    prev = dates[0]
    for k in dates:
        idx_cur = all_idx[all_idx['date'] == k].copy().drop(columns=['date','Index']).rename(columns={'Wgt':'Wgt_'+k.strftime('%Y%m%d')})
        if res.shape[0] == 0:
            res = idx_cur
        else:
            # import pdb;pdb.set_trace()
            res = res.merge(idx_cur,on='Ticker', how='outer')
            if any(res['Wgt_'+prev.strftime('%Y%m%d')].isna()):
                adds = pd.DataFrame({'Ticker': res[res['Wgt_' + prev.strftime('%Y%m%d')].isna()]['Ticker'].tolist()})
                adds['Evt'] = 'Added'
                adds['date'] = k
                changes = pd.concat([changes, adds])
            if any(res['Wgt_'+k.strftime('%Y%m%d')].isna()):
                dels = pd.DataFrame({'Ticker':res[res['Wgt_' + k.strftime('%Y%m%d')].isna()]['Ticker'].tolist()})
                dels['Evt'] = 'Deleted'
                dels['date'] = prev
                changes = pd.concat([changes, dels])    
            
        prev = k

    if volumes and len(dates)==2:
        res = idx_extra_flow(ticker, dates[0], FX=FX)
        changes = changes[['Ticker','Evt']].merge(res, on='Ticker', how='outer')
        
    return changes
            

def idx_extra_flow(idx, date_in, FX='USD'):
    save_path = create_path(IDX_path, [date_in.strftime('%Y'), date_in.strftime('%m'), date_in.strftime('%d')])
    
    dates = [date_in]
    if (date_in+ BDay(1)).date() <=dt.date.today():
        dates.append((date_in+ BDay(1)).date())
    if isinstance(idx,str):
        idx_cur = bbg_idx(idx, dates)
        idx_cur = idx_cur.drop(columns=['date'])
        save_xls = save_path +'/%s_%s_%s.xlsx' %(idx,date_in.strftime('%Y%m%d'),FX)
    else:
        idx_cur = idx
        save_xls = save_path +'/%s_%s_%s.xlsx' %('multi',date_in.strftime('%Y%m%d'),FX)
    tickers = idx_cur['Ticker'].drop_duplicates()
    
    px = last_vs_ref(tickers, 'PX_LAST', date_in, nb_ref=7*5, FX=FX)
    volume = last_vs_ref(tickers, 'VOLUME', date_in, nb_ref=7*5).drop(columns=['date'])
    auction = last_vs_ref(tickers, 'OFFICIAL_CLOSE_AUCTION_VOLUME', date_in, nb_ref=7*5).drop(columns=['date'])
    
    newcols = ['Ticker']
    for k in auction.columns:
        if k != 'Ticker':
            newcols.append(k + '_auction')
    auction.columns = newcols  
    
    res = px.merge(volume,on='Ticker', how='left',suffixes=('_px','_volume'))
    res = res.merge(auction,on='Ticker', how='left')
    
    for fldcur in ['volume', 'auction']:
        res['extra_'+ fldcur] = (res['Last_' + fldcur] / res['median_' + fldcur]) -1
        res['extra_flow_'+ fldcur] = (res['Last_' + fldcur] - res['median_' + fldcur]) * res['Last_px'] / 10**6    
        res['extra_'+fldcur+'_in_adv'] = (res['Last_' + fldcur] - res['median_' + fldcur]) / res['median_volume']
        
    res['min_size'] = res['extra_flow_'+ 'auction'] > min_flow_extra
    res = res.sort_values(by = ['min_size','extra_auction_in_adv',], ascending=[False, False]).drop(columns=['min_size'])
    res.to_excel(save_xls,index=False)
    return res
    

def last_vs_ref(ticker, fld, date_in, nb_ref=7*4, FX='USD'):
    
    data = bbg_hist(ticker, fld, [(date_in-dt.timedelta(days = nb_ref)), date_in], FX = FX)
    last_data = data[data['date'] == date_in].rename(columns={fld:'Last'})
    ref_data = data[data['date'] != date_in][['Ticker',fld]]    
        
    indic_data = ref_data.groupby(['Ticker']).min().reset_index(drop=False).rename(columns={fld:'min'})
    res = last_data.merge(indic_data, on = 'Ticker', how='left')
    
    indic_data = ref_data.groupby(['Ticker']).mean().reset_index(drop=False).rename(columns={fld:'mean'})
    res = res.merge(indic_data, on = 'Ticker', how='left')
    
    indic_data = ref_data.groupby(['Ticker']).median().reset_index(drop=False).rename(columns={fld:'median'})
    res = res.merge(indic_data, on = 'Ticker', how='left')
    
    for q in [0.05, 0.1, 0.25, 0.75, 0.9, 0.95]:
        indic_data = ref_data.groupby(['Ticker']).quantile(q=q).reset_index(drop=False).rename(columns={fld:'q_'+str(int(100*q))})
        res = res.merge(indic_data, on = 'Ticker', how='left')
    
    indic_data = ref_data.groupby(['Ticker']).max().reset_index(drop=False).rename(columns={fld:'max'})
    res = res.merge(indic_data, on = 'Ticker', how='left')    
    
    for i in [['max','min','max'], ['q_95','q_5','90'], ['q_90','q_10','80'],['q_75','q_25', '50']]:
        res['range_'+ i[2]] = 2* (res[i[0]] - res[i[1]]) / ((res[i[1]] + res[i[0]]))        
    
    return res
    
    
def get_full_float(tickers, dateIn,FX='USD'):
    free_float = bbg_hist(tickers,'EQY_FREE_FLOAT_PCT',dateIn, FX='').drop(columns=['date'])
    free_float = free_float.rename(columns= {'EQY_FREE_FLOAT_PCT':'free_float'})
    free_float['free_float'] = free_float['free_float']/100
    mcap = bbg_hist(tickers,'CUR_MKT_CAP',dateIn, FX=FX).drop(columns=['date'])
    mcap = mcap.rename(columns={'CUR_MKT_CAP':'Full_mcap'})
    mcap['Full_mcap'] = mcap['Full_mcap']/10**3
    
    res = mcap.merge(free_float, on='Ticker')
    res['ff_mcap'] = res['Full_mcap'] *res['free_float']
    return res

def get_liquidity(tickers, dateIn,FX='USD', nb_ref = 7*5):
    data = bbg_hist(tickers, ['PX_LAST','VOLUME'], [(dateIn-dt.timedelta(days = nb_ref)), dateIn], FX = FX)
    data['Turnover'] = data['PX_LAST'] * data['VOLUME'] / 10**6
    res = data[['Ticker', 'Turnover']].groupby(['Ticker']).median().reset_index(drop=False)    
    return res

def get_volatility(tickers, dateIn,FX='USD', nb_ref = 7*5):
    data = bbg_hist(tickers, ['PX_LAST'], [(dateIn-dt.timedelta(days = nb_ref)), dateIn], FX = FX)
    min_date = data['date'].min()
    shift = data.copy()
    shift = shift[shift['date']>min_date]
    prev = data.copy()
    
    prev['date'] = [(x+BDay(1)).date() for x in prev['date']]
    ret = shift.merge(prev, on =['Ticker','date'],suffixes=('','_prev'))
    ret['return'] = ret['PX_LAST'] / ret['PX_LAST_prev'] -1
    
    drift = ret[['Ticker','return']].groupby(['Ticker']).mean().reset_index(drop=False).rename(columns={'return':'drift'})
    std = ret[['Ticker','return']].groupby(['Ticker']).std().reset_index(drop=False).rename(columns={'return':'std'})
    
    res = drift.merge(std,on='Ticker')
    res['ann_vol'] = res['std'] * 260 **(1/2)
    return res  


def get_universe_data(idx,dateIn, FX = 'USD'):
    save_path = create_path(Universe_path, [dateIn.strftime('%Y'), dateIn.strftime('%m'), dateIn.strftime('%d')])
    str_flows = ''
    if dateIn<dt.date.today():
        str_flows = '_auction_flows'
    
    save_xls = save_path +'/%s_%s%s.xlsx' %(idx, dateIn.strftime('%Y%m%d'), str_flows)
    
    if os.path.isfile(save_xls):
        universe = pd.read_excel(save_xls)
    else:
    
        universe = bbg_idx(idx, dateIn)
        size = get_full_float(universe['Ticker'], dateIn,FX=FX)
        universe = universe.merge(size, on= 'Ticker')
        liq = get_liquidity(universe['Ticker'], dateIn,FX=FX, nb_ref = 7*5)
        universe = universe.merge(liq, on='Ticker')
        universe['Velocity'] = universe['Turnover'] / universe['ff_mcap'] 
        
        volatility = get_volatility(universe['Ticker'], dateIn,FX=FX, nb_ref = 7*52)
        universe = universe.merge(volatility, on='Ticker')
        
        universe = universe.sort_values(by = ['Velocity'], ascending =[ False])
        plot_scatter(universe['ff_mcap'],universe['Turnover'], universe['Velocity']/ universe['Velocity'].median(),'Size (bn)','Liquidity (m)')
        
        plot_scatter(universe['Velocity'],universe['ann_vol'], universe['ff_mcap'],'Velocity','Volatility')
        
        if dateIn<dt.date.today():
            flows = idx_extra_flow(universe, dateIn, FX=FX).drop(columns=['date'])
            universe = universe.merge(flows, on='Ticker')
            
        
        universe.to_excel(save_xls, index= False)
    
    return universe

    
