# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 11:13:26 2022

@author: User
"""

import datetime as dt
import pandas as pd
import os
import numpy as np
from tools import create_path, plot_scatter
from bbg import bbg_hist,get_universe_data
from local_paths import get_docs_path
from dates_calendar import get_quarterly_expiries

disp_path = get_docs_path() + '/BBG/dispersion'
MIN_extra_ADV = 0.35
MIN_extra_flow = 5
percentile_select = 0.75

COLS_keep = ['Index',  'date',  'Ticker',  'Idx_wgt',  'Full_mcap',  'free_float',  'ff_mcap',\
             'Turnover',  'Velocity',  'drift',  'std',  'ann_vol',\
                 'extra_flow_volume',  'extra_volume_in_adv', 'extra_flow_auction', 'extra_auction_in_adv',\
            'Size',  'ADV', 
            'rank_lt_raw',  'rank_st_raw',
            'rank_lt_disp',             'rank_st_disp',  'rank_lt',  'rank_st',        'rank_delta', 
            'scr_lt_raw',  'scr_st_raw',
            'scr_lt_disp',             'scr_st_disp',  'score_lt',  'score_st',        'score_delta',    
            'decile_lt', 'decile_st', 'decile_diff',
            'factor',  'trust',  'Wgt',  'Wgt_ADV']

def dispersion_basket(dateIn,idx, refDates, SLICE = 2, MAXSIZE = 5, MINSIZE = 0.1, MAXADV = 0.2, FX = 'USD' , uniform = False ):
    
    if max(refDates) > dateIn:
        print('Forward looking bias, assome refDates are after dateIn')
        return False
    
    save_path = create_path(disp_path,[dateIn.strftime('%Y'), dateIn.strftime('%m'), dateIn.strftime('%d')])     
    
    universe = get_universe_data(idx,dateIn, FX = FX)
    universe = universe.rename(columns={'Wgt':'Idx_wgt'})
    
    universe['Size'] = SLICE * universe['ff_mcap'] /10**3  # from bn to m
    if uniform:
        universe['Size'] = 1
        unistr = '_uniform_'
    else:
        unistr = ''
    
    save_xls = save_path+'/disp_%s%s_%s.xlsx' %(unistr, idx, dateIn.strftime('%Y%m%d'))    
    xlsw = pd.ExcelWriter(save_xls)
    
    universe.to_excel(xlsw, index = False, sheet_name='raw')
    
    
    if 'extra_auction_in_adv' in universe.columns:
        lower_adv_auction = max([MIN_extra_ADV,universe['extra_auction_in_adv'].quantile(q=percentile_select)])
        lower_adv_volume = max([MIN_extra_ADV *1.25,universe['extra_volume_in_adv'].quantile(q=percentile_select)])
        universe = universe[ ((universe['extra_auction_in_adv']>=lower_adv_auction) | (universe['extra_volume_in_adv']>=lower_adv_volume)) &\
                            (universe['extra_flow_volume']> MIN_extra_flow )].reset_index(drop=True)
    
    if universe.shape[0]==0:
        xlsw.save()
        return False    
    
    universe['ADV'] =  universe['Size'] / universe['Turnover']
    
    dates_ref = get_quarterly_expiries([dateIn- dt.timedelta(days = 7*52*1.25), dateIn])
    dates_ref =[ x  for x in dates_ref if x <min(refDates)]
    
    all_dates = dates_ref + refDates + [dateIn]
    all_dates = list(set(all_dates))
    all_dates.sort()
    
    for k in all_dates:
        try:
            px_cur = bbg_hist(universe['Ticker'], ['PX_LAST'], [k], FX = None)          
            px_cur = px_cur.rename(columns={'PX_LAST':k}).drop(columns=['date'])            
            universe = universe.merge(px_cur, on ='Ticker')
        except:
            print('PX_last, issue with date', k)            
            return
    
    for col in universe.columns:
        if universe[col].isnull().all():
            universe = universe.drop(columns=[col])
    
    cols_LT_rnk = []
    cols_ST_rnk = []
    cols_LT_scr = []
    cols_ST_scr = []
    
    for k in range(len(all_dates)):
        if all_dates[k] in universe.columns:
            if all_dates[k]!= dateIn:
                if all_dates[k] in dates_ref:
                    prefix = 'lt'
                if all_dates[k] in refDates:
                    prefix = 'st'
                fld_ret = 'ret_%s_%s'%(prefix, str(k))
                fld_rnk = 'rnk_%s_%s'%(prefix, str(k))
                fld_scr = 'scr_%s_%s'%(prefix, str(k))
                universe[fld_ret] = universe[dateIn] / universe[all_dates[k]] -1
                universe[fld_rnk] = universe[fld_ret].rank(ascending= False)
                universe[fld_scr] = (1 + universe[fld_rnk].max() - universe[fld_rnk] ) / (universe[fld_rnk].max())
                                            
                if all_dates[k] in dates_ref:
                    cols_LT_rnk.append(fld_rnk)
                    cols_LT_scr.append(fld_scr)
                if all_dates[k] in refDates:
                    cols_ST_rnk.append(fld_rnk)
                    cols_ST_scr.append(fld_scr)
            
    # # avgrank on both long and short term
    # universe['rank_lt_raw'] = universe[cols_LT_rnk].median(axis=1)
    # universe['rank_st_raw'] = universe[cols_ST_rnk].median(axis=1)
    
    # #take into account consistency over time
    # universe['rank_lt_disp'] = universe.shape[0] / (1 + universe[cols_LT_rnk].max(axis=1) - universe[cols_LT_rnk].min(axis=1))
    # universe['rank_st_disp'] = universe.shape[0] / (1 + universe[cols_ST_rnk].max(axis=1) - universe[cols_ST_rnk].min(axis=1))
    
    # for k in ['rank_lt_disp', 'rank_st_disp']:
    #     universe[k] = (universe[k] /universe[k].max()) ** 2
    
    universe['scr_lt_raw'] = universe[cols_LT_scr].median(axis=1)
    universe['scr_st_raw'] = universe[cols_ST_scr].median(axis=1)  
    
    universe['scr_lt_disp'] =  universe[cols_LT_scr].max(axis=1) - universe[cols_LT_scr].min(axis=1)
    universe['scr_st_disp'] =  universe[cols_ST_scr].max(axis=1) - universe[cols_ST_scr].min(axis=1)
    
    
    for k in ['lt','st']:
        # universe['rank_'+k] =  universe['rank_'+k+'_raw'].rank()
        universe['rank_'+k] =  universe['scr_'+k+'_raw'].rank(ascending= False)
        universe.loc[universe['rank_'+k].isna(), 'rank_'+k] = universe['rank_'+k].median()
        universe['score_'+k] = (1+ universe['rank_'+k].max() - universe['rank_'+k]  ) / universe['rank_'+k].max()   
        universe['decile_'+k] = (1 + 10 * (universe['score_'+k] -1/universe['rank_'+k].max())).astype(int)
            
    universe['decile_diff'] = (universe['decile_st'] - universe['decile_lt']) 
    universe['score_delta'] = (universe['score_st'] - universe['score_lt']) 
    
    universe['factor'] = -np.sign(universe['decile_diff']) *(universe['score_delta'])**2
    # universe['trust'] = (2* universe['rank_lt_disp'] + 1 *universe['rank_st_disp'])/3
    universe['trust'] = 1/ (2* universe['scr_lt_disp'] + 1 *universe['scr_st_disp'])/3
    universe['trust'] = universe['trust'] / universe['trust'].median()
    universe['Wgt'] = universe['Size'] * universe['factor'] * universe['trust']
    universe['Wgt_ADV'] = universe['ADV'] * universe['factor'].abs() * universe['trust']
    
    notional_Size = max(universe[universe['Size']>0]['Size'].sum(), -universe[universe['Size']<0]['Size'].sum() )
    notional_Wgt = max(universe[universe['Wgt']>0]['Wgt'].sum(), -universe[universe['Wgt']<0]['Wgt'].sum() )
    
    adj_notional = notional_Size/notional_Wgt
    if adj_notional <1:
        universe['Wgt'] = universe['Wgt'] * adj_notional
        universe['Wgt_ADV'] = universe['Wgt_ADV']* adj_notional
    
    # import pdb;pdb.set_trace()
    
    universe.loc[universe['Wgt'].abs() > MAXSIZE, 'Wgt_ADV'] = MAXSIZE *\
        (universe[universe['Wgt'].abs()>MAXSIZE]['Wgt_ADV']) / (universe[universe['Wgt'].abs()>MAXSIZE]['Wgt'].abs())
    universe.loc[universe['Wgt'].abs() > MAXSIZE, 'Wgt'] = MAXSIZE * np.sign(universe[universe['Wgt'].abs()>MAXSIZE]['Wgt'])
    
    
    universe.loc[universe['Wgt_ADV'].abs() > MAXADV, 'Wgt'] = MAXADV * (universe[universe['Wgt_ADV']>MAXADV]['Wgt']) / (universe[universe['Wgt_ADV']>MAXADV]['Wgt_ADV'])
    universe.loc[universe['Wgt_ADV'].abs() > MAXADV, 'Wgt_ADV'] = MAXADV 
    
    universe['abs_wgt'] = universe['Wgt'].abs()
    universe = universe.sort_values(by=['abs_wgt','decile_diff'], ascending=[False,False]).drop(columns=['abs_wgt'])
    COLS_ok =[]
    for k in COLS_keep:
        if k in universe.columns:
            COLS_ok.append(k)
            
    universe[COLS_ok].to_excel(xlsw,index = False, sheet_name='Select')
    universe.to_excel(xlsw,index = False, sheet_name='Full_Select')
    
    plot_scatter(universe['rank_lt'],universe['rank_st'],universe['trust'],'LT_rnk','ST_rnk')
    
    xlsw.close()
    return universe
    