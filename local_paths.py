# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 09:39:35 2022

@author: cwaki
"""
import os

def get_icloud_path():        
    res = 'C:/Users/cwaki/iCloudDrive/Python/'
    if not os.path.exists(res):
        res = 'C:/Users/User/iCloudDrive/Python/'
    return res

def get_docs_path():
    res = 'C:/Users/cwaki/Documents/'
    if not os.path.exists(res):
        res = 'C:/Users/User/Documents/'
    return res
    