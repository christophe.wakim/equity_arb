# -*- coding: utf-8 -*-
"""
Created on Wed Jul 13 09:01:25 2022

@author: User
"""

import datetime as dt
import pandas as pd
import pandas.tseries.holiday as holiday


class USCalendar(holiday.AbstractHolidayCalendar):
    rules =[
        holiday.Holiday("New Year", month=1, day=1, observance=holiday.sunday_to_monday),
        holiday.USMartinLutherKingJr,
        holiday.USPresidentsDay,
        holiday.GoodFriday,
        holiday.USMemorialDay,
        holiday.Holiday("Juneteenth", month=6, day=19, start_date="2022-06-20",observance=holiday.nearest_workday),
        holiday.Holiday("July 4th", month=7, day=4, observance=holiday.nearest_workday),
        holiday.USLaborDay,
        holiday.USThanksgivingDay,
        holiday.Holiday("Christmas", month=12, day=25, observance=holiday.nearest_workday)
        ]
    
class EUCalendar(holiday.AbstractHolidayCalendar):
    rules =[
        holiday.Holiday("New Year", month=1, day=1),       
        holiday.GoodFriday,
        holiday.EasterMonday,
        holiday.Holiday("Labour", month=5, day=1),       
        holiday.Holiday("Christmas Day", month=12, day=25),
        holiday.Holiday("Christmas Holiday", month=12, day=26)
        ]
class UKCalendar(holiday.AbstractHolidayCalendar):
    rules =[
        holiday.Holiday("New Year", month=1, day=1, observance=holiday.next_workday),       
        holiday.GoodFriday,
        holiday.EasterMonday,
        holiday.Holiday("Early May", month=5, day=1,offset=pd.DateOffset(weekday=holiday.MO(1))),
        holiday.Holiday("Late May", month=5, day=1,offset=pd.DateOffset(weekday=holiday.MO(-1))),
        holiday.Holiday("Summer", month=8, day=1,offset=pd.DateOffset(weekday=holiday.MO(-1))),
        holiday.Holiday("Christmas Day", month=12, day=25, observance=holiday.next_workday),
        holiday.Holiday('Boxing Day',month=12,day=26, observance= lambda d: d + dt.timedelta(1) if d.weekday() == 0 or d.weekday() == 6 else d )
        ]    

def select_calendar(country):
    if country.upper() =='US':
        cal = USCalendar()
    if country.upper() =='UK':
        cal = UKCalendar()
    if country.upper() =='EU':
        cal = EUCalendar()
    return cal

def get_quarterly_expiries(datesIn, country='US'):
    start = min(datesIn)
    end = max(datesIn)
    dates = pd.bdate_range(start,end,freq='BQS-MAR')
    dates_out = [third_friday_month(x) for x in dates]
    return dates_out

def get_business_day(date, next_date=1, country = 'US'):
    if isinstance(date, dt.date):
        conv_date = True
        date = dt.datetime.combine(date, dt.datetime.min.time())
    cal = select_calendar(country)
    # import pdb;pdb.set_trace()
    while date.isoweekday() > 5 or date in cal.holidays():        
        date += dt.timedelta(days=next_date)
        
    if conv_date:
        date = date.date()
    return date
    
def first_day_month(dtIn, country = 'US'):
    dtOut = get_business_day(dt.date(dtIn.year, dtIn.month, 1), next_date=1, country = country)
    return dtOut

def third_friday_month(dtIn, country = 'US'):    
    third_f =  dtIn + pd.tseries.offsets.WeekOfMonth(week=2,weekday=4)
    dtOut = get_business_day( third_f ,country=country, next_date = -1)
    
    return dtOut


def last_day_month(dtIn, country = 'US'):
    
    if dtIn.month == 12:
        nextMonth = dt.date(dtIn.year+1,1,1) 
    else:
        nextMonth = dt.date(dtIn.year,dt.month+1,1)
    last_of_month = nextMonth - dt.timedelta(days=1)
    dtOut = get_business_day(last_of_month, next_date=-1, country = country)
    
    return dtOut
    
